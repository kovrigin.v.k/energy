import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QFormLayout, QHBoxLayout, QVBoxLayout
from PyQt5.QtWidgets import QWidget, QTextEdit, QSpinBox, QLineEdit, QComboBox
from PyQt5.QtWidgets import QPushButton, QLabel, QSlider
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import Qt

physical_activity_coefficients = (1.2, 1.3, 1.6, 1.7, 1.9)
physical_activity_description = ("Минимальная активность, сидячий образ жизни",
                                 "Легкая нагрузка 1-3 раза в неделю",
                                 "Тренировки 3-5 раз в неделю",
                                 "Тренеровки ежедневно",
                                 "Тяжелая физическая работа, тренеровки 2 раза в день")


class MainWindow(QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setWindowTitle("Energy")
        self.resize(900, 600)
        self.setWindowIcon(QIcon('logo.png'))

        self.centralWidget = QWidget(self)
        self.mainLayout = QHBoxLayout(self.centralWidget)

        self.formLayout = QFormLayout()

        self.label = QLabel("Имя:")
        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label)
        self.name = QLineEdit()
        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.name)

        self.label = QLabel("Фамилия:")
        self.formLayout.setWidget(1, QFormLayout.LabelRole, self.label)
        self.secondname = QLineEdit()
        self.formLayout.setWidget(1, QFormLayout.FieldRole, self.secondname)

        self.label = QLabel("Возраст:")
        self.formLayout.setWidget(2, QFormLayout.LabelRole, self.label)
        self.age = QSpinBox()
        self.age.setRange(18, 150)
        self.formLayout.setWidget(2, QFormLayout.FieldRole, self.age)

        self.label = QLabel("Рост:")
        self.formLayout.setWidget(3, QFormLayout.LabelRole, self.label)
        self.height = QSpinBox()
        self.height.setRange(1, 300)
        self.height.setValue(170)
        self.formLayout.setWidget(3, QFormLayout.FieldRole, self.height)

        self.label = QLabel("Вес:")
        self.formLayout.setWidget(4, QFormLayout.LabelRole, self.label)
        self.weight = QSpinBox()
        self.weight.setRange(1, 300)
        self.weight.setValue(50)
        self.formLayout.setWidget(4, QFormLayout.FieldRole, self.weight)

        self.label = QLabel("Пол:")
        self.formLayout.setWidget(5, QFormLayout.LabelRole, self.label)
        self.sex = QComboBox()
        self.sex.addItems(["Мужской", "Женский"])
        self.formLayout.setWidget(5, QFormLayout.FieldRole, self.sex)

        self.label = QLabel("Формула:")
        self.formLayout.setWidget(6, QFormLayout.LabelRole, self.label)
        self.formula = QComboBox()
        self.formula.addItems(["ВОЗ", "М-С-Ж", "Х-Б"])
        self.formLayout.setWidget(6, QFormLayout.FieldRole, self.formula)

        self.label = QLabel("Активность:")
        self.formLayout.setWidget(7, QFormLayout.LabelRole, self.label)
        self.activity = QSlider(Qt.Horizontal)
        self.activity.setMinimum(0)
        self.activity.setMaximum(4)
        self.activity.setTickPosition(QSlider.NoTicks)
        self.activity.setTickInterval(1)
        self.activity.valueChanged.connect(self.activity_changed)
        self.formLayout.setWidget(7, QFormLayout.FieldRole, self.activity)

        self.label = QLabel("Цель:")
        self.formLayout.setWidget(8, QFormLayout.LabelRole, self.label)
        self.goal = QComboBox()
        self.goal.addItems(["Набор мышечной массы", "Снижение веса"])
        self.goal.currentIndexChanged.connect(self.goal_changed)
        self.formLayout.setWidget(8, QFormLayout.FieldRole, self.goal)

        self.label = QLabel("Дефицит:")
        self.formLayout.setWidget(9, QFormLayout.LabelRole, self.label)
        self.deficit = QComboBox()
        self.deficit.setEnabled(False)
        self.deficit.addItems(["10%", "20%", "30%"])
        self.formLayout.setWidget(9, QFormLayout.FieldRole, self.deficit)

        self.vbox = QVBoxLayout()
        self.vbox.addLayout(self.formLayout)

        self.calc = QPushButton("Рассчитать")
        self.calc.clicked.connect(self.calculate)
        self.vbox.addWidget(self.calc)

        self.protocol = QTextEdit()
        self.protocol.setReadOnly(True)
        self.mainLayout.addWidget(self.protocol)
        self.mainLayout.addLayout(self.vbox)
        self.centralWidget.setLayout(self.mainLayout)
        self.setCentralWidget(self.centralWidget)

        self.statusBar().showMessage("Для начала работы выберите соответствующие параметры")

    def activity_changed(self):
        act_ind = self.activity.value()
        self.statusBar().showMessage(physical_activity_description[act_ind])

    def goal_changed(self):
        if self.goal.currentIndex():  # weight reduction
            self.deficit.setEnabled(True)
        else:
            self.deficit.setEnabled(False)

    def calculate(self):
        formula = self.formula.currentIndex()  # 0 - WHO, 1 - MSJ, 2 - HB

        sex = bool(self.sex.currentIndex())   # 0 - male, 1 - female
        age = self.age.value()
        height = self.height.value()    # cm
        weight = self.weight.value()    # kg

        # bmi - body mass index
        bmi, description = self.calculate_bmi(weight, height)

        # bmr - basal metabolic rate ()
        if not formula:
            bmr = self.calculate_bmr_by_WHO(sex, age, weight)
        elif formula == 1:
            bmr = self.calculate_bmr_by_MSJ(sex, age, height, weight)
        elif formula == 2:
            bmr = self.calculate_bmr_by_HB(sex, age, height, weight)
        else:
            return

        act_ind = self.activity.value()
        daily_energy_requirement = round(bmr * physical_activity_coefficients[act_ind])

        goal = self.goal.currentIndex()  # 0 - mass gain, 1 - weight reduction
        if goal:
            deficit = (self.deficit.currentIndex() + 1) * 10  # 0 - 10%, 1 - 20%, 2 - 30%
            new_der, proteins, fats, carb = self.calculate_calories_for_weight_loss(daily_energy_requirement,
                                                                                    deficit)
        else:
            new_der, proteins, fats, carb = self.calculate_calories_for_weight_gain(daily_energy_requirement,
                                                                                    sex)

        name = self.name.text()
        secondname = self.secondname.text()
        self.protocol.clear()
        if name and secondname:
            self.protocol.append(name + " " + secondname + "\n")
        elif name or secondname:
            self.protocol.append(name + "\n" or secondname + "\n")

        self.protocol.append("Индекс массы тела: " + str(bmi) + " - " + description + "\n")
        self.protocol.append("Необходимое количество ккал в сутки: " + str(daily_energy_requirement) + "\n")
        self.protocol.append("Для " + "снижения веса:" if goal else "Для " + "набора мышечной массы:")
        self.protocol.append("Необходимое количество ккал в сутки: " + str(new_der))
        self.protocol.append("Распределение бжу (в ккал): б - "
                             + str(proteins) + " ж - " + str(fats) + " у - " + str(carb))


    def calculate_calories_for_weight_loss(self, der, deficit):
        new_der = round((der * (100 - deficit)) / 100)
        if new_der < 1200:
            proteins = round((new_der * 27) / 100)
            fats = round((new_der * 19) / 100)
            carbohydrates = round((new_der * 54) / 100)
        elif new_der < 1799:
            proteins = round((new_der * 20) / 100)
            fats = round((new_der * 30) / 100)
            carbohydrates = round((new_der * 50) / 100)
        elif new_der < 2249:
            proteins = round((new_der * 19) / 100)
            fats = round((new_der * 27) / 100)
            carbohydrates = round((new_der * 54) / 100)
        else:
            proteins = round((new_der * 19) / 100)
            fats = round((new_der * 26) / 100)
            carbohydrates = round((new_der * 55) / 100)

        return new_der, proteins, fats, carbohydrates

    def calculate_calories_for_weight_gain(self, der, sex):
        new_der = der
        new_der += 150 if sex else 250
        proteins = round((new_der * 22) / 100)
        fats = round((new_der * 36) / 100)
        carbohydrates = round((new_der * 42) / 100)

        return new_der, proteins, fats, carbohydrates

    def calculate_bmi(self, weight, height):
        bmi = round(weight/((height/100)**2), 1)
        if bmi < 19.9:
            bmi_descript = "дефецит массы тела"
        elif bmi < 24.9:
            bmi_descript = "нормальная масса тела"
        elif bmi < 29.9:
            bmi_descript = "избыточная масса тела"
        elif bmi < 34.9:
            bmi_descript = "ожирение 1 степени"
        elif bmi < 39.9:
            bmi_descript = "ожирение 2 степени"
        else:
            bmi_descript = "ожирение 3 степени"
        return bmi, bmi_descript

    def calculate_bmr_by_WHO(self, sex: bool, age: int, weight: int):
        bmr = 0
        if sex:
            if 18 <= age <= 30:
                bmr = (0.062 * weight + 2.036) * 240
            elif 31 <= age <= 60:
                bmr = (0.034 * weight + 3.538) * 240
            elif age > 60:
                bmr = (0.038 * weight + 2.755) * 240
        else:
            if 18 <= age <= 30:
                bmr = (0.063 * weight + 2.896) * 240
            elif 31 <= age <= 60:
                bmr = (0.0484 * weight + 3.653) * 240
            elif age > 60:
                bmr = (0.0491 * weight + 2.459) * 240
        return bmr

    def calculate_bmr_by_MSJ(self, sex: bool, age: int, height: int, weight: int):
        if sex:
            bmr = (9.99 * weight) + (6.25 * height) - (4.92 * age) - 161
        else:
            bmr = (9.99 * weight) + (6.25 * height) - (4.92 * age) + 5
        return bmr

    def calculate_bmr_by_HB(self, sex: bool, age: int, height: int, weight: int):
        if sex:
            bmr = 665 + (9.6 * weight) + (1.8 * height) - (4.7 * age)
        else:
            bmr = 66 + (13.7 * weight) + (5 * height) - (6.76 * age)
        return bmr


if __name__ == "__main__":

    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec_()
